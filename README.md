How To Install
--------------
1. Open terminal in Home
 
2. wget https://gitlab.com/bailabs/baitimer-deploy/raw/master/installer.sh

3. chmod +x installer.sh

4. ./installer.sh

5. cd baitimer

6. ./baitimer.sh