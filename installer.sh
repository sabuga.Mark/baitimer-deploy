#!/usr/bin/env bash
sudo apt-get install git -y
sudo apt-get install python-pip -y
sudo pip install virtualenv
sudo apt-get install libleveldb-dev -y
sudo apt-get install dpkg-dev build-essential python2.7-dev  libwebkitgtk-dev libjpeg-dev libtiff-dev libgtk2.0-dev libsdl1.2-dev libgstreamer-plugins-base* libnotify-dev freeglut3 freeglut3-dev -y
sudo apt-get install python-xlib -y
sudo apt-get install python-gobject -y
sudo apt-get install libnotify-bin -y
sudo apt-get install libnotify-dev -y


git clone https://gitlab.com/bailabs/baitimer-deploy.git baitimer
cd baitimer
virtualenv env
source env/bin/activate
ln -sf /usr/lib/python2.7/dist-packages/gi env/lib/python2.7/site-packages/
pip install --upgrade --trusted-host wxpython.org --pre -f http://wxpython.org/Phoenix/snapshot-builds/linux/3.0.3.dev2749+f803d20/ubuntu-16.04/gtk2/ wxPython_Phoenix
pip install plyvel
pip install python-xlib
wget http://mirrors.kernel.org/ubuntu/pool/main/libp/libpng/libpng12-0_1.2.54-1ubuntu1_amd64.deb
sudo dpkg -i libpng12*.deb
git clone https://github.com/frappe/frappe-client
cd frappe-client
python setup.py install
cd ..
chmod +x baitimer.sh
